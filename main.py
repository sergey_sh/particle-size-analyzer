import os
import shutil
import multiprocessing
import tkinter as tk
from typing import Any, List
from datetime import datetime

import cv2
import numpy as np
import matplotlib.pyplot as plt

from matplotlib.patches import Rectangle
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from numpy.typing import NDArray
from tkinter import ttk, filedialog, Button, messagebox, Listbox

from PSA.utils import generate_text_report
from PSA.particle_image import ParticleImage

# General
JOBS = max(multiprocessing.cpu_count() - 1, 1)
FILE_NAME = None
FILE_EXT = None
canny_image = None
partitions = None
unique_circles: List[Any] = []  # TODO inegrate to ParticleImage class
MIN_RADIUS = 10
MAX_RADIUS = 50
# TODO need separate window for setting currently hardcoded parameters
MIN_DIST = 2.5 * MIN_RADIUS
MARGIN_PCT = 5
OVERLAP = False  # TODO link deleting overlapping circles
MAX_ITER = 1000

# Params for Hough  circle transform
HOUGH_PARAM1 = 30
HOUGH_PARAM2 = 17

# Params for DBSCAN
EPS = 5.0
MIN_SAMPLES = 30
DOWNSAMPLE_DEGREE = 2  # TODO add control for it


def hyperparameter_check(image: ParticleImage) -> None:
    """
    FOR DEBUG PURPOSES ONLY.
    Quick lookup on partition number vs. DBSCAN eps/min_samples params
    """
    partitions_eps = []
    partitions_min_samples = []
    _, ax = plt.subplots(nrows=1, ncols=2)

    for eps in range(1, 20, 1):
        labels = image.dbscan(eps=eps, min_samples=30)
        partitions_eps.append(len(set(labels.flatten())))

    ax[0].scatter([range(1, 20, 1)], partitions_eps)
    ax[0].set_xlabel('epsilon')
    ax[0].set_ylabel('partitions')
    ax[0].set_title('partitions number vs. epsilon, min_samples=30')

    for min_samples in range(10, 200, 10):
        labels = image.dbscan(eps=5., min_samples=min_samples)
        partitions_min_samples.append(len(set(labels.flatten())))

    ax[1].scatter([range(10, 200, 10)], partitions_eps)
    ax[1].set_xlabel('min_samples')
    ax[1].set_ylabel('partitions')
    ax[1].set_title('partitions number vs. min_samples, epsilon=5')
    plt.show()
    return


def assess_clusters(ax0: Any, ax1: Any, src: Any, labels: NDArray,
                    cluster_start: int,
                    cluster_stop: int,
                    cluster_show: int) -> None:
    """
    FOR DEBUG PURPOSES ONLY.
    Shows a specified range of clusters
    and a rectangular frame for the specified cluster
    """
    cluster = np.argwhere(labels == 17)
    print(cluster)
    print(f'base point {(min(cluster[:, 1]), min(cluster[:, 0]))}')
    print(f'width {max(cluster[:, 0]) - min(cluster[:, 0])}')
    print(f'height {max(cluster[:, 1]) - min(cluster[:, 1])}')

    ax1.add_patch(Rectangle(
        (min(cluster[:, 1]), min(cluster[:, 0])),  # XY
        max(cluster[:, 1]) - min(cluster[:, 1]),  # Y axis?
        max(cluster[:, 0]) - min(cluster[:, 0]),  # X axis?
        fc='none',
        ec='g',
        lw=1)
        )
    ax0.text(min(cluster[:, 1]), min(cluster[:, 0]), '10', c='b')

    for i in range(0, src.image.shape[1]):
        for j in range(0, src.image.shape[0]):
            if labels[j, i] > cluster_start and labels[j, i] < cluster_stop:
                ax1.text(i, j, labels[j, i], c='g')
    return


def update_minRadius(value: Any):
    '''
    Update value for minimum radius of object.
    '''
    global MIN_RADIUS
    MIN_RADIUS = int(value)
    minRadius_text.set(value=f'min radius: {MIN_RADIUS}')
    minRadius_textbox.delete('1.0', tk.END)
    if method.get() is not None:
        update_params(None, 3, method.get())
    return


def update_maxRadius(value: Any):
    '''
    Update value for maximum radius of object.
    '''
    global MAX_RADIUS
    MAX_RADIUS = int(value)
    maxRadius_text.set(value=f'max radius: {MAX_RADIUS}')
    maxRadius_textbox.delete('1.0', tk.END)
    if method.get() is not None:
        update_params(None, 3, method.get())
    return


def update_margin(value: Any):
    '''
    Update value for additional margin (pct) which is added to partition
    to ensure that all the points belonging to a contained object will be
    accounted during RANSAC refining.
    '''
    global MARGIN_PCT
    MARGIN_PCT = int(value)
    margin_text.set(value=f'margin, pct: {MARGIN_PCT}')
    margin_textbox.delete('1.0', tk.END)
    return


def update_max_iter(value: Any):
    '''
    Update value for maximum iteration number for RANSAC.
    '''
    global MAX_ITER
    MAX_ITER = int(value)
    max_iter_text.set(value=f'max iterations: {MAX_ITER}')
    max_iter_textbox.delete('1.0', tk.END)
    return


def get_source() -> None:
    """
    Opens a dialog window to choose an image for analysis.
    The selected image gets copied into ./image folder as source_image.~ext.
    """
    # TODO clear plot from previous image if a new one is loaded
    global FILE_NAME, FILE_EXT, src
    filename = filedialog.askopenfilename(initialdir='./', title='Select File')
    if filename != '':
        FILE_NAME = filename.split('.')[0].split('/')[-1]
        FILE_EXT = filename.split('.')[-1]
        acceptable_extensions = ['jpg', 'jpeg', 'bmp', 'tiff',
                                 'tif', 'png']  # TODO to complete
        if FILE_EXT not in acceptable_extensions:
            messagebox.showerror(title=None,
                                 message='File format is not acceptable')
            return

        # remove any previous patches if they exist
        [patch.remove() for patch in ax.patches]

        shutil.copy2(filename, './image/source_image.' + FILE_EXT)
        log_listbox.insert(0, ' ' + get_time() + '  image loaded')
        src = ParticleImage(cv2.imread('./image/source_image.' + FILE_EXT))
        ax.imshow(cv2.cvtColor(src.image, cv2.COLOR_BGR2RGB))
        src.canny_edge_detection()
        image_canvas.draw()
    return


def change_method(*args) -> None:
    '''
    Reflect changes of object recognition method in GUI and call
    'update_params' function.
    '''
    if method.get() == 'Hough circles':
        slider1.configure(label='Hough param 1',
                          from_=0,
                          to=100)
        slider2.configure(label='Hough param 2')
        slider1.set(HOUGH_PARAM1)
        slider2.set(HOUGH_PARAM2)
    if method.get() == 'DBSCAN':
        slider1.configure(label='epsilon',
                          from_=0,
                          to=10)
        slider2.configure(label='minPoints')
        slider1.set(EPS)
        slider2.set(MIN_SAMPLES)
    update_params(method=method.get())
    return


def get_time() -> str:
    '''
    Returns system time as a string
    '''
    return datetime.now().strftime("%H:%M:%S")


def update_params(new_val: float = None,
                  param_num: int = None,
                  method: str = None) -> None:
    '''
    Updates parameters for the selected particles' detection method
    and partitions collection for detected particles,
    provides a visual look-up on detected particles

    Parameters
    ----------
    new_val - newly assigned value of param_num type
    param_num - simply passes type of parameter to update;
                if selected method is 'Hough circles':
                    1 - HOUGH_PARAM1
                    2 - HOUGH_PARAM2
                if selected method is 'DBSCAN':
                    1 - EPS
                    2 - MIN_SAMPLES
    method - method for object detection; currently 'Hough circles' and
             'DBSCAN' are supported.
    '''
    global HOUGH_PARAM1, HOUGH_PARAM2, EPS, MIN_SAMPLES, src

    if FILE_EXT is None:
        return

    # remove any previous patches if they exist
    [patch.remove() for patch in ax.patches]

    # retrieve params
    if method == 'Hough circles':
        if param_num == 1:
            HOUGH_PARAM1 = float(new_val)
        if param_num == 2:
            HOUGH_PARAM2 = float(new_val)

        src.detect_partitions(param1=HOUGH_PARAM1,
                              param2=HOUGH_PARAM2,
                              method='Hough circles',
                              MIN_DIST=MIN_DIST,
                              MIN_RADIUS=MIN_RADIUS,
                              MAX_RADIUS=MAX_RADIUS)

    if method == 'DBSCAN':
        if param_num == 1:
            EPS = float(new_val)
        if param_num == 2:
            MIN_SAMPLES = int(new_val)

        src.detect_partitions(param1=EPS,
                              param2=MIN_SAMPLES,
                              method='DBSCAN',
                              MIN_DIST=MIN_DIST,
                              MIN_RADIUS=MIN_RADIUS,
                              MAX_RADIUS=MAX_RADIUS,
                              DOWNSAMPLE_DEGREE=DOWNSAMPLE_DEGREE,
                              JOBS=JOBS)

    partitions = src.partitions
    if partitions is None:
        log_listbox.insert(
            0, ' ' + get_time() + '  no objects were detected')
        image_canvas.draw()
        return

    # draw detected partitions
    for partition in partitions:
        ax.add_patch(plt.Circle((partition[0],
                                partition[1]),
                                partition[2],
                     fill=False,
                     color='#55FF00'))
    # update canvas
    image_canvas.draw()
    log_listbox.insert(0, ' ' +
                       get_time() +
                       f' {method} method -' +
                       f' {partitions.shape[0]} objects detected')
    return


def refine_image() -> None:
    """
    Refines perticles shaping based on preliminary obtained partitions.
    """
    global src

    if FILE_EXT is None:
        messagebox.showerror(title=None, message='Please open the image')
        return

    if src.partitions is None:
        messagebox.showerror(title=None, message='Please select a method')
        return

    log_listbox.insert(
                0, ' ' + get_time() + '  RANSAC refining started, please wait')

    [patch.remove() for patch in ax.patches]

    src.refine_objects(
        MIN_RADIUS=MIN_RADIUS,
        MARGIN_PCT=MARGIN_PCT,
        JOBS=JOBS,
        max_iter=MAX_ITER)

    res = src.refined_circles
    # count all refined objects and visualize them
    count_refined = 0
    for obj_id, obj, mean_loss in res:
        if obj is not None:
            count_refined += 1
            ax.add_patch(plt.Circle((obj.Xc,
                                     obj.Yc),
                                    obj.radius,
                                    fill=False,
                                    color='#55FF00'))

    image_canvas.draw()
    log_listbox.insert(
                0, ' ' + get_time() + '  RANSAC refining finished, ' +
                f'{count_refined} objects refined')
    return


def save_report_to_file() -> None:
    '''
    Call a dialog window to choose location/filename to save text report.
    '''
    if len(src.refined_circles) == 0:
        messagebox.showerror(title=None,
                             message='Refine image first')
        return
    report = generate_text_report([obj[1] for obj in src.refined_circles])
    file_path = filedialog.asksaveasfilename(
        initialdir='./',
        initialfile=FILE_NAME + '_report',
        defaultextension=".txt",
        filetypes=[("Text Files", "*.txt")])
    if file_path:
        with open(file_path, "w") as f:
            f.write(report)
    return


def on_closing():
    """
    Deletes the source image from the CWD on the main window closing event.
    """
    if FILE_EXT is not None:
        try:
            os.remove('./image/source_image.' + FILE_EXT)
        except FileNotFoundError:
            pass
        plt.close()
    window.destroy()


if __name__ == '__main__':

    # create a window and populate with control elements
    window = tk.Tk()
    window.title("Particle Size Analyzer")
    window.geometry('1200x650')

    file_open_button = Button(window, text='Open image', command=get_source)
    file_open_button.place(x=10, y=10)

    method = tk.StringVar()
    method_label = tk.Label(window, text='Select detection method')
    method_label.place(x=10, y=50)
    method_select = ttk.Combobox(window,
                                 state='readonly',
                                 values=['Hough circles', 'DBSCAN'],
                                 textvariable=method)
    method_select.place(x=10, y=70)
    method.trace('w', change_method)

    minRadius_text = tk.StringVar(window, value=f'min radius: {MIN_RADIUS}')
    minRadius_label = tk.Label(window, textvariable=minRadius_text)
    minRadius_label.place(x=10, y=100)
    minRadius_textbox = tk.Text(window, height=1, width=10)
    minRadius_textbox.place(x=10, y=120)
    minRadius_textbox.bind("<Return>",
                           lambda new_val:
                           update_minRadius(
                                minRadius_textbox.get(1.0, "end-1c")))

    maxRadius_text = tk.StringVar(window, value=f'max radius: {MAX_RADIUS}')
    maxRadius_label = tk.Label(window, textvariable=maxRadius_text)
    maxRadius_label.place(x=10, y=140)
    maxRadius_textbox = tk.Text(window, height=1, width=10)
    maxRadius_textbox.place(x=10, y=160)
    maxRadius_textbox.bind("<Return>",
                           lambda new_val:
                           update_maxRadius(
                                maxRadius_textbox.get(1.0, "end-1c")))

    margin_text = tk.StringVar(window, value=f'margin, pct: {MARGIN_PCT}')
    margin_label = tk.Label(window, textvariable=margin_text)
    margin_label.place(x=10, y=180)
    margin_textbox = tk.Text(window, height=1, width=10)
    margin_textbox.place(x=10, y=200)
    margin_textbox.bind("<Return>",
                        lambda new_val:
                        update_margin(
                            margin_textbox.get(1.0, "end-1c")))

    max_iter_text = tk.StringVar(window, value=f'max iterations: {MAX_ITER}')
    max_iter_label = tk.Label(window, textvariable=max_iter_text)
    max_iter_label.place(x=10, y=220)
    max_iter_textbox = tk.Text(window, height=1, width=10)
    max_iter_textbox.place(x=10, y=240)
    max_iter_textbox.bind("<Return>",
                          lambda new_val:
                          update_max_iter(
                              max_iter_textbox.get(1.0, "end-1c")))

    refine_image_button = Button(window, text='Refine image',
                                 command=refine_image)
    refine_image_button.place(x=10, y=280)

    save_report_button = tk.Button(window,
                                   text="Save Report",
                                   command=save_report_to_file)
    save_report_button.place(x=10, y=320)

    log_listbox = Listbox(window,
                          height=39,
                          width=55,
                          bg='light goldenrod yellow',
                          activestyle='dotbox',
                          font=('Helvetica', 9),
                          fg='black')
    log_listbox.place(x=800, y=9)
    log_listbox.insert(0, ' ' + get_time() + '  OPEN THE IMAGE TO START')
    log_listbox.itemconfig(tk.END, {'fg': 'navy'})

    # set canvas for further image visualization and graph controls packing
    frame = tk.Frame(window,
                     width=602,
                     height=626,
                     highlightbackground="black",
                     highlightthickness=1)
    frame.place(x=189, y=10)
    tk_canvas = tk.Canvas(window, width=450, height=638, highlightthickness=0)
    tk_canvas.place(x=190, y=11)

    px = 1/plt.rcParams['figure.dpi']
    fig = plt.Figure(figsize=(600*px, 450*px))
    ax = fig.add_subplot()
    image_canvas = FigureCanvasTkAgg(fig, master=tk_canvas)
    image_canvas.draw()
    toolbar = NavigationToolbar2Tk(image_canvas, tk_canvas, pack_toolbar=False)
    toolbar.update()

    slider1 = tk.Scale(tk_canvas,
                       from_=1,
                       to=100,
                       orient=tk.HORIZONTAL,
                       length=400,
                       takefocus=0,
                       label="Parameter 1")

    slider2 = tk.Scale(tk_canvas,
                       from_=1,
                       to=100,
                       orient=tk.HORIZONTAL,
                       length=400,
                       takefocus=0)

    slider2.pack(side=tk.BOTTOM)
    slider1.pack(side=tk.BOTTOM)
    slider1.bind("<ButtonRelease-1>",
                 lambda new_val: update_params(slider1.get(),
                                               1,
                                               method.get()))
    slider2.bind("<ButtonRelease-1>",
                 lambda new_val: update_params(slider2.get(),
                                               2,
                                               method.get()))
    toolbar.pack(side=tk.BOTTOM, fill=tk.X)
    image_canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    window.protocol('WM_DELETE_WINDOW', on_closing)

    tk.mainloop()
