# Particle Size Analyzer
A tiny piece of software for no-code machine learning recognition and size distribution characterization of circular objects with GUI. Initially inspired by a task of quality control of size of spherical catalyst carriers, its' application may be extended to analysis of STEM/light microscopy images, cytometry, etc. In the future I plan to extend a bit its' capabilities by adding tools for multiple images batch processing, adding an API for interaction with other software, expanding range of detectable object types to other symmetry groups and adding possibility of object symmetry discovery using deep learning.

![image](./examples/GUI_example.png)
Graphical inteface of the program.
<br/><br/>

# Usage
For the task of multiple pattern recognition two options are currently implemented:
- Hough Circle Transform
- DBSCAN clustering

After selecting either of these two methods the image is divided into partitions (visualized as circles) containing supposedly one object with circular symmetry each. Depending on the chosen parameters, however, due to noise in the source image and nature of the algorithms, multiple artifact circles may be detected. This problem may be fixed by hitting the button 'Refine image', which employs Random sample consensus algorithm (RANSAC) outlier detection method along with Bullock's Least-Squares Circle Fit to refine collection of detected circles and their respective parameters. During refinement, each individual partition with its' surrounding area (defined by parameter 'margin', adding X percents to the initial partition's size) is subjected to RANSAC method which corrects the detected object's parameters or drops the partition if the algorithm does not manage to converge.
<br/><br/>

To analyze image load it using 'Open image' button, then select detection method and set up necessary parameters (to update a parameter, enter a new value to the corresponding textbox and hit 'Enter', or update the sliders below the image).
<br/><br/>

General parameters include the following:
- Min radius - minimal radius for objects
- Max radius - maximal radius for objects
- Margin, pct - defines how much of surrounding area for each partition (initially detected by either Hough tranform or DBSCAN) must be taken for RANSAC fit; the higher value, in general, will include more potentially outlying points into the data set to be refined and worsen the accuracy, however lower margin poses risk of cropping points which belong to the actual circle to be detected, which may result in RANSAC failure to converge
- Max iterations - maximum allowed number of iterations for RANSAC; low value - low chance of getting circle with acceptable mean squared error, hence, worse fit (or even failure to converge); high value - more computation time.

# Examples
![image](./examples/example_1.png)
A. An example of well adjusted Hough Circle Transform parameters (param1 = 30, param2 = 21); min/max radii are also set well, all particles have size between 10 and 50 pixels.
<br/><br/>

![image](./examples/example_2.png)
B. In this case parameters are not the optimal (Hough param 2 set to 14) and a few artifact objects are detected. It may happen even with the optimal parameters due to noise in the source image or little contrast of the objects compared to the background. At the same time, since this parameters set is more sensitive, the missing objects from the case A were detected.
<br/><br/>

![image](./examples/example_3.png)
C. Refining the image with RANSAC may provide a bit of help in fixing the case B - all artifact circles were removed while detection rate is 100%.
<br/><br/>

![image](./examples/example_4.png)
D. Result of particle detection with DBSCAN clustering (epsilon = 5, min_samples = 30). For the sake of saving the computation time, a hardcoded Gaussian pyramid kernel is applied twice to the source image to downsample it (in the future, I will add an option of adjusting it to the GUI). As a result, the detected partitions are significantly shrunk and 'margin, pct' parameter must be adjusted to a higher value, so that the real particles' edges would be captured during refining.
<br/><br/>

![image](./examples/example_5.png)
E. Result of refining of the case D image with 'margin, pct' parameter set to 60%. Detection rate - 90%.
<br/><br/>

Depending on a particular image, its' noisiness, color/contrast and background, the optimal method and set of parameters may vary, so they have to be adjusted properly to get the best result.

# Disclaimer
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Feel free to copy, use and modify for personal purposes.

# Contact

If you have any suggestions or would like to provide valuable feedback, please drop an email to [sergey.gitlab@gmail.com](mailto:sergey.gitlab@gmail.com) and make sure that the email’s subject is PARTICLE_SIZE_ANALYZER_GITLAB.





