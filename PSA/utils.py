"""
Contains function for circle refining
"""

from typing import Tuple, Union, Optional

import numpy as np

from numpy.typing import NDArray
from multiprocessing import Pool
from functools import partial
from more_itertools import zip_broadcast

from PSA.circle import Circle
from PSA.ransac_model import RansacModel
from PSA.bullock_regressor import BullockRegressor


def refine_circle(
    Xc: float,
    Yc: float,
    radius: float,
    part_id: int,
    margin_pct: float,
    image: NDArray[np.uint8],
    max_iter: int
) -> Tuple[int, Union[Circle, None], float]:
    """
    Refines parameters of a circle in a selected partition using RANSAC
    method on a data set with outliers.

    Parameters
    -----------------
    Xc, Yc - initial estimation of coordinates of center of the circle
    radius - initial estimation of the circle radius
    part_id - identificator of partition for logs
    margin_pct - defines margin of size of the partition; margin_pct with
                 value M will crop initial image to a partition of
                 [(M/100 + 1) * (estimated)radius * 2] size with (Xc,Yc) in the
                 center; the higher value, in general, will include more
                 outlying points into the data set to be refined and worsen
                 the accuracy, however lower margin poses risk of cropping
                 points which belong to the actual circle
    image_canny - a numpy array containing results of Canny edge detection on
                  original grayscale image

    Returns
    -----------------
    part_id - identificator of partition for logs
    Circle - refined circle object
    mean_loss - mean loss of RANSAC circle fitting
    """

    margin = np.uint16(np.round(radius * margin_pct / 100))
    min_x = np.uint16(np.round(max(Xc - (radius + margin), 0)))
    max_x = np.uint16(np.round(min(Xc + (radius + margin),
                      image.shape[1])))
    min_y = np.uint16(np.round(max(Yc - (radius + margin), 0)))
    max_y = np.uint16(np.round(min(Yc + (radius + margin),
                      image.shape[0])))
    cropped_canny = image[min_y: max_y, min_x: max_x]
    y, x = np.nonzero(cropped_canny)

    ransac = RansacModel(
        model=BullockRegressor,
        loss=BullockRegressor.loss,
        metric=BullockRegressor.mean_loss,
        min_points=5,
        min_inliers=100,
        max_iter=max_iter,
        threshold=2
    )

    fitted = ransac.fit(x, y)
    model = fitted.best_model
    if fitted.mean_loss == np.inf:
        return part_id, None, np.inf
    mean_loss = fitted.mean_loss
    xc = min_x + model.circle.Xc
    yc = min_y + model.circle.Yc
    radius = model.circle.radius
    return part_id, Circle(xc, yc, radius), mean_loss


def refine_circles(
    image_canny: NDArray[np.uint8],
    partitions: NDArray[np.float32],
    max_iter: int,
    n_jobs=1,
    margin_pct=5,
) -> tuple[NDArray[np.float32], list[tuple[int, Optional[Circle], float]]]:
    '''
    Refines a batch of circles with multiprocessing.

    Parameters
    -----------------
    image_canny - a numpy array containing results of Canny edge detection on
                  original grayscale image
    partitions - provides X,Y coordinates for a square partitions supposedly
                 containing individual particle each, along with half size
                 of the squares' edges.
    n_jobs - number of processors to use
    margin_pct - defines margin of size of the partition; margin_pct with
                 value M will crop initial image to a partition of
                 [(M/100 + 1) * (estimated)radius * 2] size with (Xc,Yc) in the
                 center; the higher value, in general, will include more
                 outlying points into the data set to be refined and worsen
                 the accuracy, however lower margin poses risk of cropping
                 points which belong to the actual circle

    '''

    args = [*zip_broadcast(
        partitions[:, 0],
        partitions[:, 1],
        partitions[:, 2],
        [*range(1, partitions.shape[0] + 1, 1)]
    )]

    refine_circle_partial = partial(
        refine_circle,
        margin_pct=margin_pct,
        image=image_canny,
        max_iter=max_iter
    )
    with Pool(processes=n_jobs) as pool:
        res = pool.starmap(refine_circle_partial, args)
    return res


def generate_json_report(circles: Circle,
                         pixel_size: float = 1,
                         size_unit: str = 'pixel',
                         text_format: bool = True) -> dict:
    '''
    Generate report in the form of a dictionary.
    '''
    report = {}
    report['Name'] = 'Particles size analysis'
    report['Object type'] = 'Circle'
    report['Size unit'] = size_unit

    obj_no = 1
    for circle in circles:
        if circle is not None:
            report.setdefault('objects', []).append(
                {'#': obj_no, 'diameter': circle.radius * 2 * pixel_size})
            obj_no += 1

    size_list = [radius * 2 * pixel_size for radius in Circle.radius]
    report['mean'] = np.mean(size_list)
    report['std'] = np.std(size_list)
    report['5th percentile'] = np.percentile(size_list, 5)
    report['95th percentile'] = np.percentile(size_list, 95)
    return report


def generate_text_report(circles: Circle,
                         pixel_size: float = 1,
                         size_unit: str = 'pixel') -> str:
    '''
    Generate text analysis report.
    '''
    text_report = '------------------------\n'
    text_report += 'Particles size analysis\n'
    text_report += 'Object type: Circle\n'
    text_report += f'Size unit: {size_unit}\n'
    text_report += '------------------------\n'
    obj_no = 1
    num_line = '#: {No} | diameter | {value:.2f}\n'
    for circle in circles:
        text_report += \
            num_line.format(No=obj_no, value=circle.radius * 2)
        obj_no += 1
    text_report += '------------------------\n'
    size_list = [circle.radius * 2 * pixel_size for circle in circles]
    num_line = '{param}: {value:.2f}\n'
    text_report += num_line.format(param='Mean size', value=np.mean(size_list))
    text_report += num_line.format(param='Standard deviation',
                                   value=np.std(size_list))
    text_report += num_line.format(param='5th percentile',
                                   value=np.percentile(size_list, 5))
    text_report += num_line.format(param='95th percentile',
                                   value=np.percentile(size_list, 95))
    text_report += '------------------------'
    return text_report
