"""
Contains class for regression a circle object using Bullock's method
"""

from __future__ import annotations

from dataclasses import dataclass

import numpy as np

from numpy.typing import NDArray

from PSA.circle import Circle


@dataclass
class BullockRegressor:
    """
    Implements Bullock method to find a circle with best fit for given points.

    Parameters
    ----------
    x and y : 1D arrays for points Cartesian coordinates

    Returns
    ----------
    A fitted circle objects containing center coordinates,
    radius and original data points
    """
    x: NDArray[np.float64]
    y: NDArray[np.float64]
    circle: Circle | None = None

    def fit(self) -> BullockRegressor:
        if self.x.shape != self.y.shape:
            raise ValueError("X and y must be of the same size")
        mean_x = np.mean(self.x)
        mean_y = np.mean(self.y)
        _u = self.x - mean_x
        _v = self.y - mean_y
        Suu = np.sum(np.multiply(_u, _u))
        Suv = np.sum(np.multiply(_u, _v))
        Svv = np.sum(np.multiply(_v, _v))
        Suuu = np.sum(np.multiply(np.multiply(_u, _u), _u))
        Suvv = np.sum(np.multiply(_u, np.multiply(_v, _v)))
        Svvv = np.sum(np.multiply(_v, np.multiply(_v, _v)))
        Svuu = np.sum(np.multiply(_v, np.multiply(_u, _u)))
        b1 = 0.5*(Suuu + Suvv)
        b2 = 0.5*(Svvv + Svuu)
        Uc = (b2*Suv - b1*Svv)/(Suv*Suv - Suu*Svv)
        Vc = (b1*Suv - b2*Suu)/(Suv*Suv - Suu*Svv)
        radius = (Uc**2 + Vc**2 + (Suu + Svv)/len(self.x))**0.5
        Xc = Uc + mean_x
        Yc = Vc + mean_y
        self.circle = Circle(Xc, Yc, radius)
        return self

    @property
    def Xc(self) -> float:
        return self.circle.Xc

    @property
    def Yc(self) -> float:
        return self.circle.Yc

    @property
    def radius(self) -> float:
        return self.circle.radius

    def loss(self, x, y) -> NDArray[np.float64]:
        """
        Returns an array of distances of external points to the circle
        """
        diff = np.vstack((x, y)).T - \
            np.vstack((self.circle.Xc, self.circle.Yc)).T
        radial_dist = np.linalg.norm(diff, axis=1)
        return np.absolute(radial_dist - self.radius)

    def mean_loss(self) -> float:
        return np.sum(self.loss(self.x, self.y))
