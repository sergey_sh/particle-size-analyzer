"""
Stores classes for geometric shapes.
"""

from __future__ import annotations
from dataclasses import dataclass

import numpy as np


@dataclass
class Circle(object):
    """
    Stores center coordinates and radius for a circle object
    """
    Xc: float
    Yc: float
    radius: float

    def check_overlap(self, circle: Circle, distance: float) -> bool:
        '''
        Checks if the circle object overlaps with another circle based on
        distance between their centers.
        '''
        diff = np.vstack((self.Xc, self.Yc)).T - \
            np.vstack((circle.Xc, circle.Yc)).T
        return np.linalg.norm(diff, axis=1) < distance
