"""
Contains inplementation of RANSAC method for estimating a mathematical
model from a data set with outliers
"""

from __future__ import annotations
from dataclasses import dataclass
from typing import Any
import logging

import numpy as np
from numpy.random import default_rng


logger = logging.getLogger(__name__)
rng = default_rng()


@dataclass
class RansacModel:
    '''
    Class implementing Random sample consensus (RANSAC) method.
    '''
    min_points: float = 10
    max_iter: int = 1000
    threshold: float = 3
    min_inliers: int = 10
    model: Any = None
    loss: Any = None
    metric: Any = None
    best_model: Any = None
    mean_loss: float = np.inf

    def fit(self, x, y) -> RansacModel:
        for _ in range(self.max_iter):
            ids = rng.permutation(x.shape[0])
            init_inliers = ids[: self.min_points]
            init_model = self.model(x[init_inliers], y[init_inliers]).fit()
            losses = init_model.loss(x[ids][self.min_points:],
                                     y[ids][self.min_points:])
            thresholded = (losses < self.threshold)
            also_inliers = ids[self.min_points:][np.flatnonzero(
                thresholded).flatten()]
            if also_inliers.size > self.min_inliers:
                all_inliers = np.hstack([init_inliers, also_inliers])
                new_model = self.model(x[all_inliers],
                                       y[all_inliers]).fit()
                new_error = new_model.mean_loss()
                if new_error < self.mean_loss:
                    self.mean_loss = new_error
                    self.best_model = new_model
        return self
