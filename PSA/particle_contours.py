from __future__ import annotations

from collections.abc import Iterable

import cv2

from numpy import int32, uint8
from numpy.typing import NDArray

from PSA.painter import Painter


class ParticleContours(Painter):
    def __init__(self, contours: Iterable[NDArray[int32]]) -> None:
        self.contours = contours

    def get_rectangles(self) -> ParticleContours:
        return ParticleContours([*map(
            lambda cnt: cv2.boxPoints(cv2.minAreaRect(cnt)).astype(int32),
            self.contours
        )])

    def get_ellipses(
        self
    ) -> list[tuple[tuple[float, float], tuple[float, float], float]]:
        return [
            cv2.fitEllipse(contour)
            for contour in self.contours if contour.shape[0] > 5
        ]

    def show_image(self, image: NDArray[uint8]) -> None:
        self.draw_image(
            cv2.drawContours(image, self.contours, -1, (0, 255, 0), 3)
        )
