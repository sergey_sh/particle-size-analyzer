from __future__ import annotations

from collections.abc import Callable
from typing import Any, List, Optional

import cv2
import numpy as np

from numpy import int32, int64, uint8, dstack, indices, concatenate
from numpy.typing import NDArray
from sklearn.cluster import DBSCAN

from PSA.circle import Circle
from PSA.painter import Painter
from PSA.particle_contours import ParticleContours
from PSA.utils import refine_circles


class ParticleImage(Painter):
    '''
    Main class to store a source image, Canny edge detection results,
    collection of recognized partitions (each containing an individual
    object according to the selected object recognition method) and
    collection of refined circles.
    '''
    def __init__(self, image: NDArray[uint8]) -> None:
        self.image: NDArray[uint8] = image
        self.image_canny: NDArray[uint8] = None
        self.partitions: NDArray[np.float64] = None
        self.refined_circles: List[Optional[Circle]] = []

    def apply(
        self,
        cv2_function: Callable[
            [NDArray[uint8], tuple[Any], dict[str, Any]], NDArray[uint8]
        ],
        *args: Any,
        **kwargs: Any
    ) -> ParticleImage:
        '''
        Apply given OpenCV function with specified parameters.
        '''
        return ParticleImage(cv2_function(self.image, *args, **kwargs))

    def to_grayscale(self) -> ParticleImage:
        '''
        Return grayscale image.
        '''
        return self.apply(cv2.cvtColor, cv2.COLOR_BGR2GRAY)

    def to_lab(self) -> ParticleImage:
        '''
        Transform color space to CIE LAB.
        '''
        return self.apply(cv2.cvtColor, cv2.COLOR_BGR2LAB)

    def blur(self, kernel_size: int = 5) -> ParticleImage:
        '''
        Blur an image using the normalized box filter with specified
        kernel width and height.
        '''
        return self.apply(cv2.blur, (kernel_size, kernel_size))

    def binary_threshold_inverted(self, threshold: int = 127) -> ParticleImage:
        '''
        Transform a grayscale image to a binary image using OpenCV
        THRESH_BINARY_INV type.
        '''
        image: NDArray[uint8]
        _, image = cv2.threshold(
            self.image, threshold, 255, cv2.THRESH_BINARY_INV
        )
        return ParticleImage(image)

    def canny_edge_detection(self, threshold: int = 100) -> None:
        '''
        Update 'image_canny' instance attribute with results of
        Canny edge detection.
        '''
        self.image_canny = cv2.Canny(
            cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY),
            100,
            200)

        return

    def dbscan(self, *args: Any, **kwargs: Any) -> NDArray[int64]:
        '''
        Perform DBSCAN clustering.
        '''
        x, y = self.image.shape[:2]
        coordinated_points = concatenate(
            (self.image, dstack(indices((x, y)).tolist())),
            axis=-1
        ).reshape(-1, 5)

        return DBSCAN(*args, **kwargs)\
            .fit(coordinated_points)\
            .labels_\
            .reshape(x, y)

    def get_contours(self) -> ParticleContours:
        contours: tuple[NDArray[int32]]
        contours, _ = cv2.findContours(
            self.image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )
        return ParticleContours(contours)

    def show_image(self) -> None:
        self.draw_image(self.image)

    def detect_partitions(
            self,
            param1: float = None,
            param2: float = None,
            method: str = None,
            MIN_DIST: float = 10,
            MIN_RADIUS: float = 10,
            MAX_RADIUS: float = 50,
            DOWNSAMPLE_DEGREE: int = 0,
            JOBS: int = 1) -> None:
        '''
        Detect partitions with selected method and update 'partitions'.

        Parameters
        -----------------
        param1 - HOUGH_PARAM1 if Hough circles method is selected,
                 EPS if DBSCAN clustering is selected
        param2 - HOUGH_PARAM2 if Hough circles method is selected,
                 MIN_SAMPLES if DBSCAN clustering is selected
        method - object detection method ('Hough circles' or 'DBSCAN')
        MIN_DIST - minimum allowed distance between objects centers
        MIN_RADIUS - minimum allowed radius for object
        MAX_RADIUS - maximum allowed radius for object
        DOWNSAMPLE_DEGREE - number of times image pyramid with Gaussian
                            kernel is applied to downsample image
        JOBS = numper of processors to use (needed in a case when DBSCAN method
               is selected)

        Returns
        -----------------
        Numpy array containing coordinates of centers and radii for partitions,
        assuming they have circular shape.
        '''

        # Check if Canny edge detection has been performed and do it if not
        if self.image_canny is None:
            self.canny_edge_detection()

        # Get partitions using Hough circles detection
        if method == 'Hough circles':
            # update data

            try:
                partitions = cv2.HoughCircles(self.image_canny,
                                              cv2.HOUGH_GRADIENT,
                                              dp=1,
                                              minDist=MIN_DIST,
                                              param1=param1,
                                              param2=param2,
                                              minRadius=MIN_RADIUS,
                                              maxRadius=MAX_RADIUS)[0]
                self.partitions = partitions
            except TypeError:
                self.partitions = None
                return
            return

        # Get partitions using DBSCAN clastering
        if method == 'DBSCAN':
            EPS = param1
            MIN_SAMPLES = param2

            # compress image if needed
            compressed_image = self
            for _ in range(DOWNSAMPLE_DEGREE):
                compressed_image = compressed_image.apply(cv2.pyrDown)

            # employ DBSCAN to identify partitions
            labels = compressed_image\
                .to_lab()\
                .dbscan(eps=EPS, min_samples=MIN_SAMPLES, n_jobs=JOBS)

            coeff = 2**DOWNSAMPLE_DEGREE  # upsampling coeff
            cluster_ids = np.unique(labels)[np.unique(labels) >= 0]
            partition_list = []
            for i in range(cluster_ids.shape[0]):
                cluster = np.argwhere(labels == cluster_ids[i])
                radius = max((max(cluster[:, 1]) - min(cluster[:, 1]))*coeff,
                             (max(cluster[:, 0]) - min(cluster[:, 0]))*coeff)/2
                Xc = (min(cluster[:, 1]) + max(cluster[:, 1]))*coeff/2
                Yc = (min(cluster[:, 0]) + max(cluster[:, 0]))*coeff/2
                if radius <= MAX_RADIUS and radius >= MIN_RADIUS:
                    partition_list.append(np.array([Xc, Yc, radius]))

            if len(partition_list) == 0:
                self.partitions = None
                return

            self.partitions = np.vstack(partition_list)
            return

    def refine_objects(self,
                       MIN_RADIUS: float,
                       MARGIN_PCT: float = 10,
                       JOBS: int = 1,
                       max_iter: int = 1000) -> None:
        '''
        Refine previously obtained partitions using RANSAC algorithm (which
        removes artifact objects which were mistakenly detected due to image
        noise) and remove duplicate objects (distance between the centers is
        less than MIN_RADIUS). The result is stored as attribute of a class
        instance.

        Parameters
        -----------------
        MIN_RADIUS - minimum radius for an object
        MARGIN_PCT - defines margin of size of the partition; margin_pct with
                     value M will crop initial image to a partition of
                     [(M/100 + 1) * (estimated)radius * 2] size with (Xc,Yc) in
                     the center; the higher value, in general, will include
                     more outlying points into the data set to be refined and
                     worsen the accuracy, however lower margin poses risk of
                     cropping points which belong to the actual circle and
                     RANSAC algorithm failing to converge
        JOBS - number of processors to use
        '''

        if self.partitions is None:
            return

        partitions = self.partitions
        res = refine_circles(self.image_canny,
                             partitions,
                             n_jobs=JOBS,
                             margin_pct=MARGIN_PCT,
                             max_iter=max_iter)

        # delete duplicates (circles having distance between centers less than
        # MIN_RADIUS) having worse mean loss
        # TODO some 'refined' circles have radius > MAX_RADIUS - need to remove
        unique_circles = []
        duplicate_circles = [False for obj in res]
        i = 0
        for i in range(len(res)):
            isBest = True
            if res[i][1] is None:
                continue
            obj = res[i][1]
            mean_loss = res[i][2]

            for j in range(i + 1, len(res)):
                if res[j][1] is None:
                    continue
                obj1 = res[j][1]
                mean_loss1 = res[j][2]
                if obj.check_overlap(obj1, MIN_RADIUS):
                    if mean_loss > mean_loss1:
                        isBest = False
                        break
                    else:
                        duplicate_circles[j] = True
            if isBest and not duplicate_circles[i]:
                unique_circles.append(res[i])

            i += 1

        self.refined_circles = unique_circles
        return
