import cv2

import tkinter as tk

from mypy_extensions import trait
from numpy import array, int32, uint8
from numpy.typing import NDArray


@trait
class Painter:
    @staticmethod
    def screen_resolution() -> NDArray[int32]:
        '''
        Get screen resolution
        '''
        root = tk.Tk()
        screen_width = root.winfo_screenwidth()
        screen_height = root.winfo_screenheight()
        return array(
            (screen_height, screen_width), dtype=int32
        )

    @staticmethod
    def draw_image(image: NDArray[uint8]) -> None:
        '''
        Fit image to the screen and display.
        '''
        window_name = 'image'
        window = cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        fit_ratio: float = (
            Painter.screen_resolution() * 0.9 / image.shape[:2]
        ).min()
        resized_image: NDArray[uint8] = cv2.resize(
            image,
            (int(image.shape[1] * fit_ratio), int(image.shape[0] * fit_ratio))
        )  # image a bit less than the current screen
        cv2.imshow(window, resized_image)
        cv2.waitKey()  # any key to close the window
        cv2.destroyAllWindows()
