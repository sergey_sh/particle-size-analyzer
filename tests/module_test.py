import pytest
import cv2
import numpy as np

from PSA.bullock_regressor import BullockRegressor
from PSA.ransac_model import RansacModel
from PSA.particle_image import ParticleImage


@pytest.fixture
def fitted_circle():
    x = np.array([0, 0.5, 1, 1.5, 2, 2.5, 3])
    y = np.array([0, 0.25, 1, 2.25, 4, 6.25, 9])
    return BullockRegressor(x, y).fit()


def test_bullock_fit(fitted_circle):
    assert fitted_circle.Xc == pytest.approx(-11.8393, abs=1e-4)
    assert fitted_circle.Yc == pytest.approx(8.4464, abs=1e-4)
    assert fitted_circle.radius == pytest.approx(14.6864, abs=1e-4)


def test_bullock_mean_loss(fitted_circle):
    assert fitted_circle.mean_loss() == pytest.approx(0.9413, abs=1e-4)


def test_ransac():
    np.random.seed(1)
    theta = np.linspace(0, 2*np.pi, 100)
    r = 40 + np.random.randint(1, size=100)
    x_inliers, y_inliers = r * np.cos(theta), r * np.sin(theta)
    x_outliers = np.random.randint(low=-40, high=40, size=50)
    y_outliers = np.random.randint(low=-40, high=40, size=50)
    x = np.concatenate((x_inliers, x_outliers), axis=None)
    y = np.concatenate((y_inliers, y_outliers), axis=None)
    ransac = RansacModel(model=BullockRegressor,
                         loss=BullockRegressor.loss,
                         metric=BullockRegressor.mean_loss)
    fitted = ransac.fit(x, y).best_model
    loss = fitted.mean_loss()
    assert loss < 100


@pytest.fixture
def source_image():
    src = ParticleImage(cv2.imread('./tests/silica_test_1.jpg'))
    src.canny_edge_detection()
    return src


def test_hough(source_image):
    source_image.detect_partitions(param1=30,
                                   param2=17,
                                   method='Hough circles',
                                   MIN_DIST=25,
                                   MIN_RADIUS=10,
                                   MAX_RADIUS=50)
    assert len(source_image.partitions) == 16

    # set worse Hough params to get a few phantom circles
    source_image.detect_partitions(param1=30,
                                   param2=10,
                                   method='Hough circles',
                                   MIN_DIST=25,
                                   MIN_RADIUS=10,
                                   MAX_RADIUS=50)
    assert len(source_image.partitions) == 28

    # clean phantom circles using RANSAC refining
    source_image.refine_objects(
        MIN_RADIUS=10,
        MARGIN_PCT=5,
        JOBS=1,
        max_iter=1000
    )
    assert len(source_image.refined_circles) == pytest.approx(16, abs=1)


def test_dbscan(source_image):
    source_image.detect_partitions(
        param1=6,
        param2=25,
        method='DBSCAN',
        MIN_DIST=25,
        MIN_RADIUS=10,
        MAX_RADIUS=50,
        DOWNSAMPLE_DEGREE=2,
        JOBS=1)
    assert len(source_image.partitions) == 16

    source_image.refine_objects(
        MIN_RADIUS=10,
        MARGIN_PCT=60,
        JOBS=1,
        max_iter=1000)
    assert len(source_image.refined_circles) == pytest.approx(15, abs=1)
